/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')
module.exports = {
  content: [
    './src/**/*.{vue,css}'
  ],
  theme: {
    container: {
      center: true,
      padding: {
        DEFAULT: '1rem',
        sm: '2rem',
        lg: '4rem',
        xl: '5rem',
        '2xl': '6rem',
      },
      screens: {
        lg: '1024px'
      }
    },
    extend: {
      fontFamily: {
        sans: ['IBM Plex Sans', ...defaultTheme.fontFamily.sans]
      },
      colors: {
        primary: '#0f62fe',
        secondary: '#393939',
        danger: '#DA1E28',
      },
      flex: {
        '0': '0 0 auto'
      },
      borderWidth: {
        '1': '1px'
      }
    },
  },
  plugins: [],
}


import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '@/views/LoginView.vue'
import BarangaysView from '@/views/BarangaysView.vue'
import BarangayDashboardView from '@/views/BarangayDashboardView.vue'
import IndigentRegistrationView from '@/views/IndigentRegistrationView.vue'
import NotFound from '@/views/NotFoundView.vue'
import useAuthStore from '@/stores/auth'
import { Roles } from '@/stores/user'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: LoginView,
    },
    {
      path: '/',
      name: 'barangays',
      component: BarangaysView,
      meta: {
        requiresAuth: true,
        roles: [Roles.SUPER_ADMIN],
      },
      beforeEnter: (to, from, next) => {
        const authStore = useAuthStore()
        const roles = to.meta.roles as Roles[] | undefined
        if (
          authStore.currentUser &&
          roles?.includes(authStore.currentUser.role)
        ) {
          next()
        } else {
          // TODO: transfer this checking to backend auth
          const barangay = authStore.currentUser?.barangay
          if (!barangay) {
            authStore.removeCredentials()
            next({ name: 'login' })
          } else {
            next({
              name: 'barangay-dashboard',
              params: { id: barangay._id },
            })
          }
        }
      },
    },
    {
      path: '/barangays/:id/dashboard',
      name: 'barangay-dashboard',
      component: BarangayDashboardView,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/indigent-registration/:id',
      name: 'indigent-registration',
      component: IndigentRegistrationView,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/not-found',
      name: 'NotFound',
      component: NotFound,
    },
  ],
})

router.beforeEach(async (to, from, next) => {
  const authStore = useAuthStore()
  if (to.meta.requiresAuth && !authStore.authorized) {
    next({
      name: 'login',
      query: { redirect: to.fullPath },
    })
  } else {
    next()
  }
})

export default router

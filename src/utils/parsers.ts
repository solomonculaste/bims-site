export const safelyParseJSON = <T>(value: string, fallback?: T) => {
  try {
    return JSON.parse(value) as T
  } catch (error) {
    return fallback || null
  }
}

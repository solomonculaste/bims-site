import joi from 'joi'
import { validate } from './indigent-record-validation'

export const validateBarangay = <T>(payload: T) => {
  const schema = joi.object({
    name: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    adminPassword: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    coordinates: joi
      .object({
        lat: joi.number().required(),
        lng: joi.number().required(),
      })
      .required(),
  })
  return validate(schema, payload)
}

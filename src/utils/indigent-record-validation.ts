import joi, { type ObjectSchema } from 'joi'

// TODO: refactor to its separate file
export const validate = <T>(schema: ObjectSchema, payload: T) => {
  const { value, error } = schema.validate(payload, { abortEarly: false })
  return {
    payload: value,
    errors: error
      ? Object.fromEntries(
          error.details.map(({ message, context }) => [context?.key, message])
        )
      : undefined,
  }
}

export const validateHHInformation = <T>(payload: T) => {
  const schema = joi.object({
    hhId: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    hhStatus: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    firstName: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    middleName: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    lastName: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    birthdate: joi.date().required().messages({
      'date.empty': 'FIELD_IS_REQUIRED',
      'date.base': 'FIELD_IS_REQUIRED',
    }),
    contactNumber: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    dateOfEnumeration: joi.date().required().messages({
      'date.empty': 'FIELD_IS_REQUIRED',
      'date.base': 'FIELD_IS_REQUIRED',
    }),
    extensionName: joi.string().allow(''),
  })
  return validate(schema, payload)
}

export const validateAddress = <T>(payload: T) => {
  const schema = joi.object({
    address: joi
      .string()
      .required()
      .messages({ 'string.empty': 'ADDRESS_IS_REQUIRED' }),
    coordinates: joi.object({
      lat: joi.number().required(),
      lng: joi.number().required(),
    }),
  })
  return validate(schema, payload)
}

export const validateHousehold = <T>(payload: T) => {
  const schema = joi.object({
    recommendation: joi
      .string()
      .required()
      .messages({ 'string.empty': 'RADIO_REQUIRED_ERROR_MESSAGE' }),
    recommendationCategory: joi.any().when('recommendation', {
      is: joi.exist().equal('RECOMMENDATION_CHOICE_06'),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'RADIO_REQUIRED_ERROR_MESSAGE' }),
    }),
    remarks: joi.string().allow(''),
  })
  return validate(schema, payload)
}

export const validateHHMember = <T>(payload: T) => {
  const schema = joi.object({
    _id: joi.string().allow(''),
    memberType: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    memberStatus: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    firstName: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    middleName: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    lastName: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    extensionName: joi.string().allow(''),
    birthdate: joi.date().required().messages({
      'date.empty': 'FIELD_IS_REQUIRED',
      'date.base': 'FIELD_IS_REQUIRED',
    }),
    age: joi
      .number()
      .min(0)
      .required()
      .messages({ 'number.empty': 'FIELD_IS_REQUIRED' }),
    gender: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    declaredPregnancyStatus: joi.any().when('gender', {
      is: joi.exist().equal('F'),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    pregnancyStatus: joi.any().when('gender', {
      is: joi.exist().equal('F'),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    lmp: joi.any().when('pregnancyStatus', {
      is: joi.exist().equal('PREGNANCY_STATUS_CODE_01'),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    pwd: joi.boolean().required().messages({
      'boolean.required': 'FIELD_IS_REQUIRED',
      'boolean.base': 'FIELD_IS_REQUIRED',
    }),
    relationshipToHH: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    soloParent: joi.boolean().required().messages({
      'boolean.required': 'FIELD_IS_REQUIRED',
      'boolean.base': 'FIELD_IS_REQUIRED',
    }),
    maritalStatus: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    ipMember: joi.boolean().required().messages({
      'boolean.required': 'FIELD_IS_REQUIRED',
      'boolean.base': 'FIELD_IS_REQUIRED',
    }),
    ipCustomName: joi.any().when('ipMember', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    mothersMaidenName: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    hhGrantee: joi.boolean().required().messages({
      'boolean.required': 'FIELD_IS_REQUIRED',
      'boolean.base': 'FIELD_IS_REQUIRED',
    }),
    visitingHF: joi.any().when('age', {
      is: joi.number().max(5),
      then: joi.boolean().required().messages({
        'boolean.required': 'FIELD_IS_REQUIRED',
        'boolean.base': 'FIELD_IS_REQUIRED',
      }),
    }),
    reasonForNotVisitingHF: joi.any().when('visitingHF', {
      is: joi.exist().equal(false),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    hfName: joi.any().when('visitingHF', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    hfAddress: joi.any().when('visitingHF', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    hfId: joi.any().when('visitingHF', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    philSysCardNo: joi.any().when('visitingHF', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    cvsEducation: joi.boolean().required().messages({
      'boolean.required': 'FIELD_IS_REQUIRED',
      'boolean.base': 'FIELD_IS_REQUIRED',
    }),
    attendingSchool: joi.boolean().required().messages({
      'boolean.required': 'FIELD_IS_REQUIRED',
      'boolean.base': 'FIELD_IS_REQUIRED',
    }),
    reasonForNotAttendingSchool: joi.any().when('attendingSchool', {
      is: joi.exist().equal(false),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    schoolName: joi.any().when('attendingSchool', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    schoolAddress: joi.any().when('attendingSchool', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    schoolFacilityId: joi.any().when('attendingSchool', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    lrn: joi.any().when('attendingSchool', {
      is: joi.exist().equal(true),
      then: joi
        .string()
        .required()
        .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    }),
    education: joi.string().allow(''),
  })
  return validate(schema, payload)
}

export const validateFinalStep = <T>(payload: T) => {
  const schema = joi.object({
    validatorSignature: joi.string().allow(''),
    validatorName: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    immediateSupervisorSignature: joi.string().allow(''),
    immediateSupervisorName: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    immediateSupervisorPosition: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
    beneficiarySignature: joi.string().allow(''),
    beneficiaryName: joi
      .string()
      .required()
      .messages({ 'string.empty': 'FIELD_IS_REQUIRED' }),
  })
  return validate(schema, payload)
}

export type CustomModal =
  | (HTMLElement & { open: () => void; close: () => void })
  | null
export type FieldType =
  | 'text'
  | 'password'
  | 'textarea'
  | 'select'
  | 'date'
  | 'checkbox'
  | 'radio'
  | 'toggle'
  | 'gmap-autocomplete'
export type FieldValue = string | number | boolean
export interface FieldOption {
  value: FieldValue
  label: string
}
export type FieldErrors<T> = {
  [key in keyof Partial<T>]: string
}

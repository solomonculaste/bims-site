import { defineStore } from 'pinia'
import { ref } from 'vue'
import axios from 'axios'
import * as interceptors from '@/config/axios-interceptors'
import type { Barangay } from './barangay'

export enum Roles {
  SUPER_ADMIN = 'SUPER_ADMIN',
  ADMIN = 'ADMIN',
}

export interface User {
  _id: string
  username: string
  password?: string
  email?: string
  firstName?: string
  lastName?: string
  role: Roles
  barangay?: Barangay
}

export type Query = Partial<Omit<User, 'barangay'>> & {
  limit?: number
  page?: number
}

const api = axios.create({
  baseURL: import.meta.env.VITE_BIMS_API_BASE_URL,
  headers: { 'Content-Type': 'application/json' },
})
api.interceptors.request.use(interceptors.onRequest)
api.interceptors.response.use(undefined, interceptors.onResponseError)

export default defineStore('user', () => {
  const loading = ref(false)
  const error = ref<string | null>(null)

  async function fetch() {
    try {
      loading.value = true
      error.value = null
      return (await api.get<User[]>('/users')).data
    } catch (e) {
      error.value = 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  async function update({ _id, ...payload }: Partial<User>) {
    try {
      loading.value = true
      error.value = null
      return (await api.put<User[]>(`/users/${_id}`, payload)).data
    } catch (e) {
      error.value = 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  return {
    loading,
    error,
    fetch,
    update,
  }
})

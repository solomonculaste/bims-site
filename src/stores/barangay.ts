import { defineStore } from 'pinia'
import { ref } from 'vue'
import axios from 'axios'
import * as interceptors from '@/config/axios-interceptors'
import type { User } from './user'

export interface Barangay {
  _id: string
  name: string
  administrator?: User
  coordinates: {
    lat: number
    lng: number
  }
}

const api = axios.create({
  baseURL: import.meta.env.VITE_BIMS_API_BASE_URL,
  headers: { 'Content-Type': 'application/json' },
})
api.interceptors.request.use(interceptors.onRequest)
api.interceptors.response.use(undefined, interceptors.onResponseError)

export default defineStore('barangay', () => {
  const loading = ref(false)
  const error = ref<string | null>(null)
  const barangays = ref<Barangay[]>([])

  async function fetch(id?: Barangay['_id']) {
    try {
      loading.value = true
      error.value = null
      const { data } = await api.get<Barangay[] | Barangay>(
        '/barangays' + (id ? `/${id}` : '')
      )
      // we don't store the result to state if it is not a single query
      if (!id) barangays.value = data as Barangay[]
      else return data as Barangay
    } catch (e) {
      error.value = 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  async function register(payload: Omit<Barangay, '_id'>) {
    try {
      loading.value = true
      error.value = null
      return (await api.post<Barangay>('/barangays/new', payload)).data
    } catch (e) {
      error.value = 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  return {
    loading,
    error,
    barangays,
    fetch,
    register,
  }
})

import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import axios, { type AxiosError } from 'axios'
import { type User } from './user'
import { safelyParseJSON } from '@/utils/parsers'

export interface LoginPayload {
  username: string
  password: string
}

export interface Credentials {
  user: User
  token: string
}

const { VITE_BIMS_API_BASE_URL, VITE_BIMS_LS_TOKEN_KEY } = import.meta.env

const api = axios.create({
  baseURL: VITE_BIMS_API_BASE_URL,
  headers: { 'Content-Type': 'application/json' },
})

export default defineStore('auth', () => {
  const currentUser = ref<User | null>(null)
  const authToken = ref<string | null>(null)
  const loading = ref(false)
  const error = ref<string | null>()

  const authorized = computed(() =>
    Boolean(authToken.value && currentUser.value)
  )

  async function authenticate(payload: LoginPayload) {
    try {
      error.value = null
      loading.value = true
      const { data } = await api.post<Credentials>('/auth/login', payload)
      authToken.value = data.token
      currentUser.value = data.user
      localStorage.setItem(VITE_BIMS_LS_TOKEN_KEY, JSON.stringify(data))
    } catch (e) {
      error.value =
        (e as AxiosError).response?.status === 401
          ? 'Invalid credentials'
          : 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  async function retrieveCredentials() {
    return new Promise((resolve) => {
      const item = localStorage.getItem(VITE_BIMS_LS_TOKEN_KEY)
      const credentials = safelyParseJSON<Credentials>(item as string)
      if (credentials) {
        authToken.value = credentials.token
        currentUser.value = credentials.user
      }
      resolve(true)
    })
  }

  function removeCredentials() {
    authToken.value = null
    currentUser.value = null
    localStorage.removeItem(VITE_BIMS_LS_TOKEN_KEY)
  }

  return {
    currentUser,
    authToken,
    authorized,
    authenticate,
    retrieveCredentials,
    removeCredentials,
    error,
    loading,
  }
})

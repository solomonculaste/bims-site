import { ref } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import * as interceptors from '@/config/axios-interceptors'

export interface Household {
  _id: string
  firstName: string
  middleName: string
  lastName: string
  extensionName?: string
  birthdate: string
  gender: string
  memberStatus: string
  memberType: string
  declaredPregnancyStatus: string
  pregnancyStatus: string
  lmp: string
  pwd: boolean
  relationshipToHH: string
  soloParent: boolean
  maritalStatus: string
  ipMember: string
  ipCode: string
  ipName: string
  ipCustomName: string
  mothersMaidenName: string
  hhGrantee: boolean
  visitingHF: boolean
  reasonForNotVisitingHF: string
  hfName: string
  hfAddress: string
  hfId: string
  philSysCardNo: string
  cvsEducation: boolean
  attendingSchool: string
  reasonForNotAttendingSchool: string
  schoolName: string
  schoolFacilityId: string
  schoolAddress: string
  lrn: string
  education: string
}

const api = axios.create({
  baseURL: import.meta.env.VITE_BIMS_API_BASE_URL,
  headers: { 'Content-Type': 'application/json' },
})
api.interceptors.request.use(interceptors.onRequest)
api.interceptors.response.use(undefined, interceptors.onResponseError)

export default defineStore('householdStore', () => {
  const loading = ref(false)
  const error = ref<string | null>(null)

  async function save(payload: Partial<Household>[]) {
    try {
      loading.value = true
      error.value = null
      return (await api.post<Household[]>('/households/new', payload)).data
    } catch (e) {
      error.value = 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  async function batchUpdate(payload: Partial<Household>[]) {
    try {
      loading.value = true
      error.value = null
      return (await api.put<Household[]>('/households/batch-update', payload))
        .data
    } catch (e) {
      error.value = 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  async function remove(id: string) {
    try {
      loading.value = true
      error.value = null
      return await api.delete(`/households/${id}`)
    } catch (e) {
      error.value = 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  return { loading, error, save, batchUpdate, remove }
})

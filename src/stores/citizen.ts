import { ref } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import * as interceptors from '@/config/axios-interceptors'
import type { Barangay } from './barangay'
import type { Household } from './household'

export interface Citizen {
  _id: string
  __v?: string
  createdAt?: string
  updatedAt?: string
  firstName: string
  middleName: string
  lastName: string
  extensionName?: string
  birthdate: string
  contactNumber: string
  address: string
  previousAddresses?: string
  barangay: Barangay
  coordinates: {
    lat: number
    lng: number
  }
  hhId: string
  hhStatus: string
  recommendation: string
  recommendationCategory?: string
  dateOfEnumeration: string
  remarks?: string
  householdMembers?: Household[]
  validatorName: string
  validatorSignature: string
  immediateSupervisorName: string
  immediateSupervisorSignature: string
  immediateSupervisorPosition: string
  beneficiaryName: string
  beneficiarySignature: string
  revoked?: boolean
}

export type NewCitizen = Omit<
  Citizen,
  '_id' | 'barangay' | 'householdMembers'
> & {
  _id?: string
  barangayId?: string
  hhMemberIds: string[]
}

export enum StepIds {
  INFORMATION,
  ADDRESS,
  VALIDATION,
  ROSTER_VALIDATION,
  MEMBERS,
  CHILD_FROM_SUCCEEDING_PREGNANCY,
  FINAL_NOTE_AND_SIGNATURES,
}

export type Query = Partial<Omit<Citizen, 'barangay'>> & {
  barangay?: string
  limit?: number
  page?: number
  keywords?: string
}

const api = axios.create({
  baseURL: import.meta.env.VITE_BIMS_API_BASE_URL,
  headers: { 'Content-Type': 'application/json' },
})
api.interceptors.request.use(interceptors.onRequest)
api.interceptors.response.use(undefined, interceptors.onResponseError)

const store = defineStore('citizenStore', () => {
  const citizens = ref<Citizen[]>([])
  const loading = ref(false)
  const error = ref<string | null>(null)
  const selectedCitizen = ref<Citizen | null>()

  async function fetch(query: Query) {
    try {
      loading.value = true
      error.value = null
      const { data } = await api.get<Citizen[]>('/citizens', { params: query })
      citizens.value = data
    } catch (e) {
      error.value = 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  async function filtered(query: Query) {
    try {
      loading.value = true
      error.value = null
      const { data } = await api.get<Citizen[]>('/citizens/filtered', {
        params: query,
      })
      citizens.value = data
    } catch (e) {
      error.value = 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  async function getCitizen(id: string) {
    try {
      loading.value = true
      error.value = null
      const { data } = await api.get<Citizen>(`/citizens/${id}`)
      return data
    } catch (e) {
      error.value = 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  async function register(payload: NewCitizen) {
    try {
      loading.value = true
      error.value = null
      await api.post<Citizen>('/citizens/new', payload)
    } catch (e) {
      error.value = 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  async function update(payload: Partial<Citizen>) {
    try {
      loading.value = true
      error.value = null
      await api.put<Citizen>(`/citizens/${payload._id}`, payload)
    } catch (e) {
      error.value = 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  async function deleteRecord() {
    try {
      loading.value = true
      error.value = null
      if (!selectedCitizen.value) throw new Error()
      await api.delete(`/citizens/${selectedCitizen.value._id}`)
    } catch (e) {
      error.value = 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  // TODO: separate to reports store
  async function fetchStatusesReport(barangayId: string) {
    try {
      loading.value = true
      error.value = null
      const { data } = await api.get<{ [key: string]: number }>(
        `/reports/bar/status/${barangayId}`
      )
      return data
    } catch (e) {
      error.value = 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  async function checkHits(payload: Partial<Citizen>) {
    try {
      loading.value = true
      error.value = null
      const { data } = await api.post<Citizen[]>(
        '/citizens/check-hits',
        payload
      )
      return data
    } catch (e) {
      error.value = 'Something went wrong'
    } finally {
      loading.value = false
    }
  }

  return {
    citizens,
    selectedCitizen,
    fetch,
    filtered,
    getCitizen,
    register,
    update,
    fetchStatusesReport,
    deleteRecord,
    checkHits,
    loading,
    error,
  }
})

export default store

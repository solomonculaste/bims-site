import '@/assets/main.css'
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import i18n, { loadLanguageAsync } from './config/i18n'
import useAuthStore from '@/stores/auth'
import VueGoogleMaps from '@fawmi/vue-google-maps'
import '@googlemaps/js-api-loader'
// @ts-ignore
import VueSignaturePad from 'vue-signature-pad'

import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(i18n)
app.use(VueSignaturePad)
;(async () => {
  app.use(VueGoogleMaps, {
    load: {
      key: import.meta.env.VITE_GMAP_API_KEY,
      libraries: 'places',
    },
  })
  const authStore = useAuthStore()
  await authStore.retrieveCredentials()
  await loadLanguageAsync('EN')
  app.mount('#app')
})()

import { type InternalAxiosRequestConfig, type AxiosError } from 'axios'
import useAuthStore from '@/stores/auth'

export const onRequest = (config: InternalAxiosRequestConfig) => {
  const authStore = useAuthStore()
  if (config.headers) config.headers.Authorization = authStore.authToken
  return config
}

export const onResponseError = (error: AxiosError) => {
  if (window.location.pathname !== 'login' && error.response?.status === 401) {
    window.location.replace('/login?redirect=' + window.location.pathname)
  }
  return Promise.reject(error)
}

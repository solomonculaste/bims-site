import { createI18n } from 'vue-i18n'
import axios from 'axios'
const i18n = createI18n({
  locale: 'EN', // set locale
  fallbackLocale: 'EN', // set fallback locale
  silentTranslationWarn: true,
})

const setI18nLanguage = (lang: string) => {
  i18n.global.locale = lang
  axios.defaults.headers.common['Accept-Language'] = lang
  const html = document.querySelector('html')
  if (html) html.setAttribute('lang', lang)
  return lang
}

const loadLanguageAsync = async (lang: 'EN' | 'TL') => {
  const { data } = await axios.get<{ [key: string]: string }>(
    `${import.meta.env.VITE_BIMS_API_BASE_URL}/misc/translations/${lang}`
  )
  i18n.global.setLocaleMessage(lang, data)
  setI18nLanguage(lang)
}

export default i18n
export { loadLanguageAsync }
